import os
from flask import Flask, Response
app = Flask(__name__)

@app.route('/')
def main():
    """
        Hello
    """
    for key in os.environ.keys():
        print ("%30s %s " % (key,os.environ[key]))
    return 'Hello, world!'

@app.route('/health')
def health():
    return Response("Python38 is up.", status=200)
